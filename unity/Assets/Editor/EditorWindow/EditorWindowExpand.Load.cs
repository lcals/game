﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEditor;

namespace Editor
{
    public partial class EditorWindowExpand
    {
        protected static List<T> GetDirectivesFromXmlFile<T>()
        {
            var serializer = new XmlSerializer(typeof(List<T>));
            using TextReader reader = new StreamReader(GetXmlAssetPath);
            var directives = (List<T>) serializer.Deserialize(reader);

            return directives;
        }

        protected static void SaveDataToXmlFile<T>(List<T> directives)
        {
            var serializer = new XmlSerializer(typeof(List<T>));
            using TextWriter writer = new StreamWriter(GetXmlAssetPath);
            serializer.Serialize(writer, directives);
            AssetDatabase.Refresh();
        }

        
    }
}