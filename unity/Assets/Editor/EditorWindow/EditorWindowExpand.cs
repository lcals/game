﻿using UnityEditor;

namespace Editor
{
    public abstract partial class EditorWindowExpand : EditorWindow
    {
        protected abstract void OnEnable();
    }
}