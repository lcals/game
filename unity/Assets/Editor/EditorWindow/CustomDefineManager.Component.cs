﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace Editor
{
    public abstract partial class EditorWindowExpand
    {
        protected static GUIStyle InitTitleStyle(string windowName, Color guiColor)
        {
            var titleStyle = new GUIStyle(EditorStyles.toolbar)
            {
                fixedHeight = 0, padding = new RectOffset(8, 8, 0, 0)
            };

            var headerStyle = new GUIStyle(EditorStyles.largeLabel)
            {
                fontStyle = FontStyle.Bold, normal = {textColor = Color.white}
            };

            GUI.color = new Color(0.5f, 0.5f, 0.5f);

            EditorGUILayout.BeginHorizontal(titleStyle, GUILayout.Height(20));
            {
                GUI.color = guiColor;
                EditorGUILayout.LabelField("", GUILayout.Width(31));
                EditorGUILayout.LabelField(windowName, headerStyle, GUILayout.Height(20));
            }
            EditorGUILayout.EndHorizontal();
            return titleStyle;
        }

        protected static void RenderTableHeader(IEnumerable<(string name, int Width, int Height)> tables,
            Color guiColor)
        {
            var style = new GUIStyle(EditorStyles.toolbar)
            {
                fontStyle = FontStyle.Bold, fontSize = 12, alignment = TextAnchor.MiddleCenter, fixedHeight = 0
            };

            GUI.color = new Color(0.5f, 0.5f, 0.5f);

            EditorGUILayout.BeginHorizontal(style, GUILayout.Height(20));

            GUI.color = guiColor;

            EditorGUILayout.LabelField("", GUILayout.Width(31));

            foreach (var table in tables)
            {
                GUILayout.Space(4);
                EditorGUILayout.LabelField(table.name, style, GUILayout.Width(table.Width),
                    GUILayout.Height(table.Height));
            }


            GUILayout.FlexibleSpace();

            EditorGUILayout.EndHorizontal();
        }

        protected static void RenderTableEnd(IEnumerable<(string name, int Width, int Height, UnityAction unityEvent)> ends, Color guiColor)
        {
            var style = new GUIStyle(EditorStyles.miniButtonMid)
            {
                fontStyle = FontStyle.Bold, fontSize = 12, alignment = TextAnchor.MiddleCenter, fixedHeight = 0
            };
            GUI.color = new Color(0.75f, 0.75f, 0.75f);
            EditorGUILayout.BeginHorizontal(style, GUILayout.Height(24), GUILayout.ExpandWidth(true));
            {
                GUI.color = guiColor;

                GUILayout.Label("", GUILayout.Width(31));

                foreach (var end in ends)
                {
                    GUILayout.Space(4);
                    if (GUILayout.Button(end.name, GUILayout.Width(end.Width), GUILayout.Height(end.Height)))
                    {
                        end.unityEvent?.Invoke();
                    }
                }
            }
            EditorGUILayout.EndHorizontal();
        }
    }
}