using System.Collections.Generic;
using System.IO;
using GameComponent;
using GameData;
using NUnit.Framework;

public class FlatBuffersTest
{
    [Test]
    public void FlatBuffers_Gen_Json()
    {
        var rootDataT = new RootDataT();
        var scenesT = new ScenesT {ScenesType = Scenes_Type.Main};
        rootDataT.Scenes = new List<ScenesT> {scenesT};
        var testT = new TestT();
        var testT1 = new TestT {EnumTest1 = enum_Test1.Test1 | enum_Test1.Test2};
        rootDataT.Test = new List<TestT> {testT,testT1};
        var stringKeyT = new StringKeyT {Key = 1, Value = "1"};
        var stringKeyT1= new StringKeyT();
        rootDataT.StringKey = new List<StringKeyT> {stringKeyT,stringKeyT1};
        File.WriteAllText("Assets/dep/FlatBuffersManager/Editor/Test.json", rootDataT.SerializeToJson());
    }
    
}