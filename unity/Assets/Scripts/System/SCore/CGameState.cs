﻿using System;
using System.Linq;
using Cysharp.Threading.Tasks;
using GameData;
using Microsoft.Extensions.Logging;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.SceneManagement;
using ZLogger;
using Component = Frame.Component;

namespace GameComponent
{
    public enum GameState
    {
        Init,
        LoadLoginScenes,
        Login,
        Loading,
        MainScenes,
        Function,
        OtherScenes,
        Disconnect,
    }

    public class CGameState : Component, IGameState
    {
        private GameState _oldState;
        private GameState _gameState;
        private readonly ICData _cData;
        private readonly ICAssetLoad _cAssetLoad;


        public CGameState(ICData cData, ICAssetLoad cAssetLoad)
        {
            _gameState = GameState.Init;
            _cData = cData;
            _cAssetLoad = cAssetLoad;
        }


        public override void Init()
        {
        }

        public override void Open()
        {
        }

        public override void Execute()
        {
            switch (_gameState)
            {
                case GameState.Init:
                    if (_cData.LoadDataSuccess())
                    {
                        SetGameState(GameState.LoadLoginScenes);
                    }

                    break;
                case GameState.LoadLoginScenes:
                    OnLoadLoginScenes();
                    break;
                case GameState.Login:
                    break;
                case GameState.Loading:
                    break;
                case GameState.MainScenes:
                    break;
                case GameState.Function:
                    break;
                case GameState.OtherScenes:
                    break;
                case GameState.Disconnect:
                    break;
            }
        }


        public override void Close()
        {
        }

        public override void Destroy()
        {
        }


        public void SetGameState(GameState gameState)
        {
            _oldState = _gameState;
            CLog.GlobalLogger.ZLog(LogLevel.Trace, "Cur State :{0} Next State :{1}", _gameState, gameState);
            _gameState = gameState;
        }

        private void OnLoadLoginScenes()
        {
            if (_oldState != GameState.Init) return;
            _oldState = GameState.LoadLoginScenes;
            var scenesT = _cData.GetDataT().Scenes.FirstOrDefault(scene => scene.ScenesType == Scenes_Type.Login);

            if (scenesT != null)
            {
                var obj = _cAssetLoad.LoadSceneAsync(scenesT.NameId.GetDataString(), (o) =>
                {
                    switch (o.Status)
                    {
                        case AsyncOperationStatus.Succeeded:
                            SetGameState(GameState.Login);
                            break;
                        default:
                            CLog.GlobalLogger.ZLogError("Failed To LoadScene  type:{0} status:{1} ",
                                nameof(Scenes_Type.Login), o.Status);
                            break;
                    }
                }, LoadSceneMode.Additive);
            }
            else
            {
                CLog.GlobalLogger.ZLogError("The Current Type Of Scene Does Not Exist  type:{0}",
                    nameof(Scenes_Type.Login));
            }
        }
    }
}