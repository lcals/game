﻿namespace GameComponent
{
    public interface IGameState
    {

        public void SetGameState(GameState gameState);
    }
}