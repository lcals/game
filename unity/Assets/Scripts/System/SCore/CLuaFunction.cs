﻿using System;
using UnityEngine;
using XLua;
using ZLogger;
using Component = Frame.Component;

namespace GameComponent
{
    public class CLuaFunction : Component, ICLuaFunction
    {
        private LuaEnv _luaEnv;

        public override void Init()
        {
            _luaEnv = new LuaEnv();
            try
            {
                _luaEnv.DoString(@"
                xlua.hotfix(CS.GameComponent.CLuaFunction, 'TestLuaFix', function(self)
                   CS.UnityEngine.Debug.Log('fix code success');
                end)
            ");
            }
            catch (Exception e)
            {
                CLog.GlobalLogger.ZLogError(e.Message);
            }
        }

        public override void Open()
        {
            TestLuaFix();
        }

        public override void Execute()
        {
            _luaEnv?.Tick();
        }

        public override void Close()
        {
        }

        public override void Destroy()
        {
            _luaEnv.Dispose();
        }

        public void TestLuaFix()
        {
            Debug.LogError("fix code failure");
        }
    }
}