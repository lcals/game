﻿using System;
using System.Collections.Generic;
using Other;
using UnityEngine;
using XLua;


public static class XLuaGenConfig
{
    [LuaCallCSharp] public static List<Type> LuaCallCSharp = new List<Type>()
    {
        typeof(UnityEngine.Debug),
        typeof(GameComponent.CLuaFunction),
        typeof(Other.LuaFix),
        typeof(System.Span<byte>),
    };
    
    [Hotfix] public static List<Type> HotfixList = new List<Type>()
    {
        typeof(GameComponent.CLuaFunction),
        typeof(Other.LuaFix)
    };
}