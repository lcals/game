﻿using GameComponent;

namespace GameSystem
{
    public class SCore : Frame.GameSystem
    {
        public SCore(GameSystemManager gameSystemManager)
        {
            GameSystemManager = gameSystemManager;
        }

        public override string GetGameSystemName()
        {
            return nameof(SCore);
        }

        public override void InitGameSystem()
        {
            AddComponent<CLuaFunction>();
            AddComponent<CGameState>();
        }

        public override void DestroyGameSystem()
        {
            RemoveComponents();
        }
    }
}