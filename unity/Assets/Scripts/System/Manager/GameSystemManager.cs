﻿namespace GameSystem
{
    public class GameSystemManager : Frame.GameSystemManager
    {
        public override string GetGameSystemManagerName()
        {
            return nameof(GameSystemManager);
        }
    }
}