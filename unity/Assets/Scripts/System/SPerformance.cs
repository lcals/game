﻿namespace GameSystem
{
    public class SPerformance : Frame.GameSystem
    {
        public SPerformance(GameSystemManager gameSystemManager)
        {
            GameSystemManager = gameSystemManager;
        }
        public override string GetGameSystemName()
        {
            return nameof(SPerformance);
        }

        public override void InitGameSystem()
        {
        }

        public override void DestroyGameSystem()
        {
            RemoveComponents();
        }
    }
}