﻿using GameComponent;

namespace GameSystem
{
    public class SResources : Frame.GameSystem
    {
        public SResources(GameSystemManager gameSystemManager)
        {
            GameSystemManager = gameSystemManager;
        }

        public override string GetGameSystemName()
        {
            return nameof(SResources);
        }

        public override void InitGameSystem()
        {
        }

        public override void DestroyGameSystem()
        {
            RemoveComponents();
        }
    }
}