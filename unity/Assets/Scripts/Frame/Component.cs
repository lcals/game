﻿namespace Frame
{
    public abstract class Component
    {
        public string Name;
        public abstract void Init();
        public abstract void Open();
        public abstract void Execute();
        public abstract void Close();
        public abstract void Destroy();
    }
}