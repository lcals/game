﻿using System.Collections.Generic;
using GameComponent;

namespace Frame
{
    public abstract class GameSystem : Component
    {
        private static Injector _serviceContainer;
        private readonly Dictionary<string, Component> _components = new Dictionary<string, Component>();
        protected GameSystemManager GameSystemManager;
        public static Injector Services => _serviceContainer ??= InitializeStaticServices();

        public abstract string GetGameSystemName();

        public abstract void InitGameSystem();

        public abstract void DestroyGameSystem();

        private static Injector InitializeStaticServices()
        {
            var injector = new Injector();
            injector.AddInjectionForType<ICEnvironment, CEnvironment>();
            injector.AddInjectionForType<ICLog, CLog>();
            injector.AddInjectionForType<ICData, CData>();
            injector.AddInjectionForType<ICAssetLoad, CAssetLoad>();
            return injector;
        }


        protected void AddComponent<T>() where T : Component
        {
            var strName = typeof(T).ToString();
            var component = Services.GetInjectionFor<T>();
            GameSystemManager.AddComponent(strName, component);
            _components.Add(strName, component);
        }

        protected void RemoveComponent<T>() where T : Component
        {
            var strName = typeof(T).ToString();
            GameSystemManager.RemoveComponent(strName);
            _components.Remove(strName);
        }

        protected void RemoveComponents()
        {
            foreach (var vKey in _components.Keys) GameSystemManager.RemoveComponent(vKey);
            _components.Clear();
        }


        #region LifeCycle

        public override void Init()
        {
            foreach (var module in _components.Values) module?.Init();
        }

        public override void Open()
        {
            foreach (var module in _components.Values) module?.Open();
        }

        public override void Execute()
        {
            foreach (var module in _components.Values) module?.Execute();
        }

        public override void Close()
        {
            foreach (var module in _components.Values) module?.Close();
        }

        public override void Destroy()
        {
            foreach (var module in _components.Values) module?.Destroy();
        }

        #endregion
    }
}