﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Frame
{
    public class Injector
    {
        private readonly Dictionary<Type, Type> _implementationFromInterface = new Dictionary<Type, Type>();
        private readonly Dictionary<Type, object> _savedInstances = new Dictionary<Type, object>();

        public T GetInjectionFor<T>()
        {
            return (T) GetInstanceOf(typeof(T));
        }

        public void AddInjectionForType<TKey, TImplementation>()
        {
            AddAddInjectionForType(typeof(TKey), typeof(TImplementation));
        }

        private Type GetClassFromInterface(Type type)
        {
            if (_implementationFromInterface.ContainsKey(type)) return _implementationFromInterface[type];

            throw new KeyNotFoundException($"No class implementation defined for interface type '{type}'");
        }

        private Type GetClassTypeFrom(Type type)
        {
            Type typeFrom;
            if (type.IsClass)
            {
                typeFrom = type;
            }
            else if (type.IsInterface)
            {
                typeFrom = GetClassFromInterface(type);
            }
            else
            {
                if (!_savedInstances.ContainsKey(type))
                    throw new NotImplementedException($"GetClassTypeFrom for Type '{type}' not implemented!");
                typeFrom = type;
            }

            return typeFrom;
        }

        private object NewInstanceOf(Type type)
        {
            var constructorInfo =
                type.GetConstructors(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                    .FirstOrDefault();
            object[] array = null;
            if (constructorInfo != null)
            {
                array = new object[constructorInfo.GetParameters().Length];
                foreach (var parameterInfo in constructorInfo.GetParameters())
                {
                    var instanceOf = GetInstanceOf(parameterInfo.ParameterType);
                    array[parameterInfo.Position] = instanceOf;
                }
            }

            return !(array ?? Array.Empty<object>()).Any()
                ? Activator.CreateInstance(type)
                : Activator.CreateInstance(type, array);
        }

        private object GetInstanceOf(Type type)
        {
            var classTypeFrom = GetClassTypeFrom(type);
            if (_savedInstances.TryGetValue(classTypeFrom, out var value)) return value;

            var obj = NewInstanceOf(classTypeFrom);
            _savedInstances.Add(classTypeFrom, obj);
            return obj;
        }


        private void AddAddInjectionForType(Type keyType, Type implementationClass)
        {
            if (_implementationFromInterface.ContainsKey(keyType)) _implementationFromInterface.Remove(keyType);

            _implementationFromInterface.Add(keyType, implementationClass);
        }
    }
}