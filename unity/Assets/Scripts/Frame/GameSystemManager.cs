﻿using System;
using System.Collections.Generic;

namespace Frame
{
    public abstract class GameSystemManager : Component
    {
        private readonly Dictionary<string, Component> _components = new Dictionary<string, Component>();
        private readonly Dictionary<string, GameSystem> _gameSystems = new Dictionary<string, GameSystem>();
        public abstract string GetGameSystemManagerName();


        #region Component

        public T GetComponent<T>() where T : Component
        {
            return _GetComponent<T>();
        }

        public Component GetComponent(string strName)
        {
            return _GetComponent(strName);
        }

        private T _GetComponent<T>() where T : Component
        {
            var component = _GetComponent(typeof(T).ToString());
            if (component == null) throw new Exception(" _GetComponent  is null");

            return component as T;
        }

        private Component _GetComponent(string strName)
        {
            _components.TryGetValue(strName, out var component);
            return component;
        }

        public T AddComponent<T>() where T : Component
        {
            var strName = typeof(T).ToString();
            var component = GameSystem.Services.GetInjectionFor<T>();
            component.Name = strName;
            _components.Add(strName, component);
            component.Init();
            component.Open();
            return component;
        }

        public void AddComponent(string strName, Component component)
        {
            component.Name = strName;
            _components.Add(strName, component);
            component.Init();
            component.Open();
        }

        public void RemoveComponent<T>() where T : Component
        {
            var strName = typeof(T).ToString();
            if (!_components.TryGetValue(strName, out var component)) return;
            _components.Remove(strName);
            component.Close();
            component.Destroy();
        }

        public void RemoveComponent(string strName)
        {
            if (!_components.TryGetValue(strName, out var component)) return;
            _components.Remove(strName);
            component.Close();
            component.Destroy();
        }

        public void RemoveComponents()
        {
            _components.Clear();
            foreach (var component in _components.Values)
            {
                component.Close();
                component.Destroy();
            }
        }

        #endregion

        #region GameSystem

        public T GetGameSystem<T>() where T : GameSystem
        {
            return _GetGameSystem<T>();
        }

        public Component GetGameSystem(string strName)
        {
            return _GetGameSystem(strName);
        }

        private T _GetGameSystem<T>() where T : GameSystem
        {
            var gameSystem = _GetGameSystem(typeof(T).ToString());
            if (gameSystem == null) throw new Exception(" _GetGameSystem  is null");

            return gameSystem as T;
        }

        private Component _GetGameSystem(string strName)
        {
            _gameSystems.TryGetValue(strName, out var gameSystem);
            return gameSystem;
        }

        public void AddGameSystem<T>(GameSystem gameSystem) where T : GameSystem
        {
            var strName = typeof(T).ToString();
            _gameSystems.Add(strName, gameSystem);
            gameSystem.InitGameSystem();
        }

        public T AddGameSystem<T>() where T : GameSystem
        {
            var strName = typeof(T).ToString();
            var gameSystem = GameSystem.Services.GetInjectionFor<T>();
            gameSystem.Name = strName;
            _gameSystems.Add(strName, gameSystem);
            gameSystem.InitGameSystem();
            return gameSystem;
        }

        public void AddGameSystem(string strName, GameSystem gameSystem)
        {
            gameSystem.Name = strName;
            _gameSystems.Add(strName, gameSystem);
            gameSystem.InitGameSystem();
        }

        public void RemoveGameSystem<T>() where T : GameSystem
        {
            var strName = typeof(T).ToString();
            if (!_gameSystems.TryGetValue(strName, out var gameSystem)) return;
            _gameSystems.Remove(strName);
            gameSystem.InitGameSystem();
        }

        public void RemoveGameSystem(string strName)
        {
            if (!_gameSystems.TryGetValue(strName, out var gameSystem)) return;
            _gameSystems.Remove(strName);
            gameSystem.InitGameSystem();
        }

        public void RemoveGameSystems()
        {
            _gameSystems.Clear();
            foreach (var gameSystem in _gameSystems.Values) gameSystem.DestroyGameSystem();
        }

        #endregion

        #region LifeCycle

        public override void Init()
        {
            foreach (var module in _gameSystems.Values) module?.Init();
        }

        public override void Open()
        {
            foreach (var module in _gameSystems.Values) module?.Open();
        }

        public override void Execute()
        {
            foreach (var module in _gameSystems.Values) module?.Execute();
        }

        public override void Close()
        {
            foreach (var module in _gameSystems.Values) module?.Close();
        }

        public override void Destroy()
        {
            foreach (var module in _gameSystems.Values) module?.Destroy();
        }

        #endregion
    }
}