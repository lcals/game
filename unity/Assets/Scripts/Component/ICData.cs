﻿using System.Collections.Generic;
using GameData;

namespace GameComponent
{
    public interface ICData
    {
        public bool LoadDataSuccess();
        public void LoadData();
        public void UpdateDate();
        public RootDataT GetDataT();
    }
}