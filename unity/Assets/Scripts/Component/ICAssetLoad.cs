﻿using System;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

namespace GameComponent
{
    public interface ICAssetLoad
    {
        public void LoadAssetAsync<TObject>(object key, Action<AsyncOperationHandle<TObject>> completed);
        public bool ReleaseInstance(AsyncOperationHandle handle);

        public AsyncOperationHandle<SceneInstance> LoadSceneAsync(object key,
            Action<AsyncOperationHandle<SceneInstance>> completed,
            LoadSceneMode loadMode = LoadSceneMode.Single,
            bool activateOnLoad = true, int priority = 100);

        public void UnloadSceneAsync(AsyncOperationHandle<SceneInstance> handle,
            Action<AsyncOperationHandle<SceneInstance>> completed,
            bool autoReleaseHandle = true);
    }
}