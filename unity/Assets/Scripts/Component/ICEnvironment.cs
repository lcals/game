﻿namespace GameComponent
{
    public interface ICEnvironment
    {
        public void SetTargetFrameRate(Fps fps = Fps.Low);
        public int GetTargetFrameRate();
        public Fps GetFps();
    }
}