﻿using System;
using UnityEngine;
using Component = Frame.Component;

namespace GameComponent
{
    public enum Fps
    {
        Low,
        Middle,
        High,
        Free
    }

    public class CEnvironment : Component, ICEnvironment
    {
        public CEnvironment()
        {
            SetTargetFrameRate();
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }

        private Fps FPS { get; set; } = Fps.Low;


        public void SetTargetFrameRate(Fps fps = Fps.Low)
        {
            FPS = fps;
            Application.targetFrameRate = fps switch
            {
                Fps.Low => 30,
                Fps.Middle => 45,
                Fps.High => 60,
                Fps.Free => -1,
                _ => throw new ArgumentOutOfRangeException(nameof(fps), fps, null)
            };
        }

        public int GetTargetFrameRate()
        {
            return Application.targetFrameRate;
        }

        public Fps GetFps()
        {
            return FPS;
        }


        public override void Init()
        {
        }

        public override void Open()
        {
        }

        public override void Execute()
        {
        }

        public override void Close()
        {
        }

        public override void Destroy()
        {
        }
    }
}