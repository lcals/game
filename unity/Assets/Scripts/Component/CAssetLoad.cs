﻿using System;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using ZLogger;
using Component = Frame.Component;

namespace GameComponent
{
    [Serializable]
    public class AssetReferenceMaterial : AssetReferenceT<Material>
    {
        public AssetReferenceMaterial(string guid) : base(guid)
        {
        }
    }


    public class CAssetLoad : Component, ICAssetLoad
    {
        public CAssetLoad()
        {
            Addressables.InitializeAsync();
        }

        public void LoadAssetAsync<TObject>(object key, Action<AsyncOperationHandle<TObject>> completed)
        {
            Addressables.LoadAssetAsync<TObject>(key).Completed += completed;
        }

        public bool ReleaseInstance(AsyncOperationHandle handle)
        {
            if (Addressables.ReleaseInstance(handle)) return true;
#if UNITY_EDITOR
            CLog.GlobalLogger.ZLogError("ReleaseInstance Error handle Info:{0}",
                Newtonsoft.Json.JsonConvert.SerializeObject(handle, Newtonsoft.Json.Formatting.Indented));
#endif
            return false;
        }

        public AsyncOperationHandle<SceneInstance> LoadSceneAsync(object key,
            Action<AsyncOperationHandle<SceneInstance>> completed,
            LoadSceneMode loadMode = LoadSceneMode.Single,
            bool activateOnLoad = true, int priority = 100)
        {
            var operationHandle = Addressables.LoadSceneAsync(key, loadMode, activateOnLoad, priority);
            operationHandle.Completed += completed;
            return operationHandle;
        }

        public void UnloadSceneAsync(AsyncOperationHandle<SceneInstance> handle,
            Action<AsyncOperationHandle<SceneInstance>> completed,
            bool autoReleaseHandle = true)
        {
            Addressables.UnloadSceneAsync(handle, autoReleaseHandle).Completed += completed;
        }

        ~CAssetLoad()
        {
        }


        public override void Init()
        {
        }

        public override void Open()
        {
        }

        public override void Execute()
        {
        }

        public override void Close()
        {
        }

        public override void Destroy()
        {
        }
    }
}