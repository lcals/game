﻿using Cysharp.Text;
using Microsoft.Extensions.Logging;
using UnityEngine;
using ZLogger;
using Component = Frame.Component;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace GameComponent
{
    public class CLog : Component, ICLog
    {
        private readonly ILoggerFactory _loggerFactory;

        public static ILogger GlobalLogger;

        public CLog()
        {
            CreateLogger("Global", out _loggerFactory, out GlobalLogger);
            GlobalLogger.ZLogInformation("Init Global Log");
        }

        public void CreateLogger(string fileName, out ILoggerFactory factory, out ILogger logger)
        {
            factory = UnityLoggerFactory.Create(builder =>
            {
                builder.SetMinimumLevel(LogLevel.Trace);
#if ENABLE_LOGS
                builder.AddZLoggerRollingFile(
                    (offset, i) =>
                    {
#if UNITY_STANDALONE_WIN||UNITY_EDITOR
                        const string path = "game_log";
#else
                        var path = ZString.Format("{0}/{1}", Application.persistentDataPath, "game_log");
#endif
                        var name = ZString.Format("{0}/{1}/{2}_{3}.log", path, fileName,
                            offset.ToLocalTime().ToString("yyyy-MM-dd"),
                            i.ToString("0000"));

                        return name;
                    },
                    offset => offset.ToLocalTime().Date, 1024, x =>
                    {
                        x.PrefixFormatter = (writer, info) =>
                            ZString.Utf8Format(writer, "[{0}][{1}]", info.Timestamp.ToLocalTime().DateTime,
                                info.CategoryName);
                    });
#endif
#if ENABLE_DEBUG
                builder.AddZLoggerUnityDebug();
#endif
            });
            logger = factory.CreateLogger(fileName);
            Application.quitting += factory.Dispose;
        }

        public void CreateLogger<T>(out ILogger<T> logger)
        {
            if (GlobalLogger == null) Init();

            GlobalLogger.ZLogInformation(ZString.Format("Init {0} log", typeof(T).ToString()));
            logger = _loggerFactory.CreateLogger<T>();
        }

        private ILogger<T> GetLogger<T>() where T : class
        {
            return _loggerFactory.CreateLogger<T>();
        }


        public override void Init()
        {
        }

        public override void Open()
        {
        }

        public override void Execute()
        {
        }

        public override void Close()
        {
        }

        public override void Destroy()
        {
        }
    }
}