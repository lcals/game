﻿using Microsoft.Extensions.Logging;

namespace GameComponent
{
    public interface ICLog
    {
        public void CreateLogger(string fileName, out ILoggerFactory factory, out ILogger logger);
        public void CreateLogger<T>(out ILogger<T> logger);
    }
}