﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GameData;
using UnityEngine;
using ZLogger;
using Component = Frame.Component;

namespace GameComponent
{
    public static class CDataExtensions
    {
        private static Dictionary<int, string> _stringKey;
        public static string GetDataString(this int id)
        {
            if (_stringKey == null)
            {
                _stringKey = new Dictionary<int, string>();
                var cData = Frame.GameSystem.Services.GetInjectionFor<ICData>();
                var rootDataT = cData.GetDataT();
                foreach (var vKeyT in rootDataT.StringKey)
                {
                    _stringKey.Add(vKeyT.Key,vKeyT.Value);
                }
            }
#if ENABLE_DEBUG
            if (_stringKey.TryGetValue(id ,out var value))
            {
                return value;
            }
            CLog.GlobalLogger.ZLogError("TheSpecifiedIdDoesNotExist");
            throw new Exception("TheSpecifiedIdDoesNotExist");
#else
            return _stringKey[id];
#endif

        }
    }

    public partial class CData : Component, ICData
    {
        private readonly ICAssetLoad _cAssetLoad;


        private RootDataT _rootDataT;
        private bool _loadDataSuccess;


        public CData(ICAssetLoad cAssetLoad)
        {
            _cAssetLoad = cAssetLoad;
            LoadData();
        }


        public bool LoadDataSuccess()
        {
            return _loadDataSuccess;
        }


        public void LoadData()
        {
#if Read_External_Data
            _rootDataT =
                RootDataT.DeserializeFromBinary(
                    File.ReadAllBytes(Path.GetFullPath("../unity_data/test_data/RootData.bytes")));
            LoadDataSuccess = true;
            CLog.GlobalLogger.ZLogInformation("Load Data Success");
#else
            _cAssetLoad.LoadAssetAsync<TextAsset>(data,
                o =>
                {
                    _rootDataT = RootDataT.DeserializeFromBinary(o.Result.bytes);
                    _loadDataSuccess = true;
                    CLog.GlobalLogger.ZLogInformation("Load Data Success  DataVersion:{0}",_rootDataT.Version);
                    _cAssetLoad.ReleaseInstance(o);
                });
#endif
        }

        public void UpdateDate()
        {
#if Read_External_Data
            _rootDataT =
                RootDataT.DeserializeFromBinary(
                    File.ReadAllBytes(Path.GetFullPath("../unity_data/test_data/RootData.bytes")));
            LoadDataSuccess = true;
            CLog.GlobalLogger.ZLogInformation("Load Data Success");
#else
            _cAssetLoad.LoadAssetAsync<TextAsset>(data,
                o =>
                {
                    _rootDataT = RootDataT.DeserializeFromBinary(o.Result.bytes);
                    _loadDataSuccess = true;
                    _cAssetLoad.ReleaseInstance(o);
                });
#endif
        }


        public RootDataT GetDataT()
        {
            return _rootDataT;
        }

        public override void Init()
        {
        }

        public override void Open()
        {
        }

        public override void Execute()
        {
        }

        public override void Close()
        {
        }

        public override void Destroy()
        {
        }
    }
}