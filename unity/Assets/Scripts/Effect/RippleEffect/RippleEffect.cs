﻿using UnityEngine;

namespace GameEffect
{
    public class RippleEffect : MonoBehaviour
    {
        public AnimationCurve Waveform = new AnimationCurve(
            new Keyframe(0.00f, 0.50f, 0, 0),
            new Keyframe(0.05f, 1.00f, 0, 0),
            new Keyframe(0.15f, 0.10f, 0, 0),
            new Keyframe(0.25f, 0.80f, 0, 0),
            new Keyframe(0.35f, 0.30f, 0, 0),
            new Keyframe(0.45f, 0.60f, 0, 0),
            new Keyframe(0.55f, 0.40f, 0, 0),
            new Keyframe(0.65f, 0.55f, 0, 0),
            new Keyframe(0.75f, 0.46f, 0, 0),
            new Keyframe(0.85f, 0.52f, 0, 0),
            new Keyframe(0.99f, 0.50f, 0, 0)
        );

        [Range(0.01f, 1.0f)] public float RefractionStrength = 0.5f;

        public Color ReflectionColor = Color.gray;

        [Range(0.01f, 1.0f)] public float ReflectionStrength = 0.7f;

        [Range(1.0f, 3.0f)] public float WaveSpeed = 1.25f;

        [Range(0.0f, 2.0f)] public float DropInterval = 0.5f;

        [SerializeField, HideInInspector] private Shader Shader;

        private class Droplet
        {
            private Vector2 _position;
            private float _time;

            public Droplet()
            {
                _time = 1000;
            }

            public void Reset()
            {
                _position = new Vector2(Random.value, Random.value);
                _time = 0;
            }

            public void Update()
            {
                _time += Time.deltaTime;
            }

            public Vector4 MakeShaderParameter(float aspect)
            {
                return new Vector4(_position.x * aspect, _position.y, _time, 0);
            }
        }

        private Droplet[] _droplets;
        private Texture2D _gradTexture;
        private Material _material;
        private float _timer;
        private int _dropCount;
        private static readonly int GradTex = Shader.PropertyToID("_GradTex");
        private static readonly int Drop1 = Shader.PropertyToID("_Drop1");
        private static readonly int Drop2 = Shader.PropertyToID("_Drop2");
        private static readonly int Drop3 = Shader.PropertyToID("_Drop3");
        private static readonly int Reflection = Shader.PropertyToID("_Reflection");
        private static readonly int Params1 = Shader.PropertyToID("_Params1");
        private static readonly int Params2 = Shader.PropertyToID("_Params2");

        private void UpdateShaderParameters()
        {
            var c = GetComponent<Camera>();

            _material.SetVector(Drop1, _droplets[0].MakeShaderParameter(c.aspect));
            _material.SetVector(Drop2, _droplets[1].MakeShaderParameter(c.aspect));
            _material.SetVector(Drop3, _droplets[2].MakeShaderParameter(c.aspect));

            _material.SetColor(Reflection, ReflectionColor);
            _material.SetVector(Params1, new Vector4(c.aspect, 1, 1 / WaveSpeed, 0));
            _material.SetVector(Params2, new Vector4(1, 1 / c.aspect, RefractionStrength, ReflectionStrength));
        }

        private void Awake()
        {
            _droplets = new Droplet[3];
            _droplets[0] = new Droplet();
            _droplets[1] = new Droplet();
            _droplets[2] = new Droplet();

            _gradTexture = new Texture2D(2048, 1, TextureFormat.Alpha8, false)
            {
                wrapMode = TextureWrapMode.Clamp, filterMode = FilterMode.Bilinear
            };
            for (var i = 0; i < _gradTexture.width; i++)
            {
                var x = 1.0f / _gradTexture.width * i;
                var a = Waveform.Evaluate(x);
                _gradTexture.SetPixel(i, 0, new Color(a, a, a, a));
            }

            _gradTexture.Apply();

            _material = new Material(Shader) {hideFlags = HideFlags.DontSave};
            _material.SetTexture(GradTex, _gradTexture);

            UpdateShaderParameters();
        }

        private void Update()
        {
            if (DropInterval > 0)
            {
                _timer += Time.deltaTime;
                while (_timer > DropInterval)
                {
                    Emit();
                    _timer -= DropInterval;
                }
            }

            foreach (var d in _droplets) d.Update();

            UpdateShaderParameters();
        }

        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            Graphics.Blit(source, destination, _material);
        }

        private void Emit()
        {
            _droplets[_dropCount++ % _droplets.Length].Reset();
        }
    }
}