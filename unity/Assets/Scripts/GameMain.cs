﻿using System;
using Cysharp.Threading.Tasks;
using GameComponent;
using GameSystem;
using Other;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Networking;

public class GameMain : MonoBehaviour
{
    private static GameMain _instance;
    private ICAssetLoad _cAssetLoad;
    private ICEnvironment _cEnvironment;
    private ICData _cData;
    private ICLog _cLog;


    private GameSystemManager _gameSystemManager;


    private async void Awake()
    {
        try
        {
            var luaCode = (await UnityWebRequest.Get("http://173.82.19.18/test.fix").SendWebRequest()).downloadHandler
                .text;
            gameObject.AddComponent<LuaFix>().Init(luaCode);
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
        finally
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
            _gameSystemManager = Frame.GameSystem.Services.GetInjectionFor<GameSystemManager>();
#if ENABLE_DEBUG
            EnableDebug();
#endif
            AddComponent();
            AddGameSystem();
        }
    }
    private void Start()
    {
        _gameSystemManager?.Init();
    }

    private void Update()
    {
        _gameSystemManager?.Execute();
    }

    private void OnEnable()
    {
        _gameSystemManager?.Open();
    }

    private void OnDisable()
    {
        _gameSystemManager?.Close();
    }

    private void OnDestroy()
    {
        _gameSystemManager?.Destroy();
    }

    public static GameMain Instance()
    {
        return _instance;
    }
#if ENABLE_DEBUG
    private static void EnableDebug()
    {
        var test = new Test();
        test.OnTest();
        Addressables.InitializeAsync();
        Addressables.LoadAssetAsync<GameObject>(CData.debug).Completed += obj =>
        {
            var o = Instantiate(obj.Result);
            o.AddComponent<FPSGUI>();
        };
    }
#endif
    private void AddComponent()
    {

        _cLog = _gameSystemManager.AddComponent<CLog>();
        _cEnvironment = _gameSystemManager.AddComponent<CEnvironment>();
        _cAssetLoad = _gameSystemManager.AddComponent<CAssetLoad>();
        _cData = _gameSystemManager.AddComponent<CData>();
        
    }

    private void AddGameSystem()
    {
        _gameSystemManager.AddGameSystem<SCore>();
        _gameSystemManager.AddGameSystem<SPerformance>();
        _gameSystemManager.AddGameSystem<SResources>();
    }
}