﻿using System.Globalization;
using UnityEngine;

namespace Other
{
    public class FPSGUI : MonoBehaviour
    {
        private float _fps;
        private int _frameCount;
        private float _prevTime;

        private void Start()
        {
            _frameCount = 0;
            _prevTime = 0.0f;
        }

        private void Update()
        {
            ++_frameCount;
            var time = Time.realtimeSinceStartup - _prevTime;
            if (!(time >= 0.5f)) return;
            _fps = _frameCount / time;

            _frameCount = 0;
            _prevTime = Time.realtimeSinceStartup;
        }

        private void OnGUI()
        {
            var gUIStyle = GUIStyle.none;
            gUIStyle.fontSize = 60;
            gUIStyle.normal.textColor = Color.red;
            gUIStyle.alignment = TextAnchor.UpperLeft;
            var rect = new Rect(40, 0, 100, 100);
            GUI.Label(rect, _fps.ToString(CultureInfo.InvariantCulture), gUIStyle);
        }
    }
}