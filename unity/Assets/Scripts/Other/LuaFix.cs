﻿using System;
using UnityEngine;
using XLua;

namespace Other
{
  
    public class LuaFix : MonoBehaviour
    {
        private LuaEnv _luaEnv;

        public void Init(string luaCode)
        {
            _luaEnv = new LuaEnv();
            _luaEnv.DoString(luaCode);
            TestLuaFix();
        }
        public void Update()
        {
            _luaEnv?.Tick();
        }

        private static void TestLuaFix()
        {
            Debug.LogError("first fix code failure");
        }
    }
}