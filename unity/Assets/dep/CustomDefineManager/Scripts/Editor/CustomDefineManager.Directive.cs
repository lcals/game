﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CustomDefineManagement
{
    [Serializable]
    public class Directive
    {
        [SerializeField]
        public string Name;
        
        [SerializeField]
        public CustomDefineManager.CdmBuildTargetGroup Targets;

        [SerializeField]
        public bool Enabled = true;
        
        [SerializeField]
        public int SortOrder;

        public override string ToString()
        {
            return $"{Name} : {Targets.ToString()}";
        }
    }
}