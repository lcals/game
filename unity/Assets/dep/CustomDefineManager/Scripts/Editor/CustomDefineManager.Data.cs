﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CustomDefineManagement
{
    public partial class CustomDefineManager
    {
        private static List<Directive> LoadDirectives()
        {
            var directives = new List<Directive>();

            foreach (CdmBuildTargetGroup platform in Enum.GetValues(typeof(CdmBuildTargetGroup)))
            {
                var platformSymbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(platform.ToBuildTargetGroup());

                if (string.IsNullOrEmpty(platformSymbols)) continue;
                foreach (var symbol in platformSymbols.Split(';'))
                {
                    var directive = directives.FirstOrDefault(d => d.Name == symbol);

                    if (directive == null)
                    {
                        directive = new Directive { Name = symbol };
                        directives.Add(directive);
                    }

                    directive.Targets |= platform;
                }
            }

            var dataFileDirectives = GetDirectivesFromXmlFile<Directive>();

            if (directives.Any())
            {
                if (!dataFileDirectives.Any())
                {
                    SaveDirectives(directives);
                }                                
            }

            // Add any directives from the data file which weren't located in the configuration file
            directives.AddRange(dataFileDirectives.Where(df => directives.All(d => d.Name != df.Name)));

            foreach (var dataFileDirective in dataFileDirectives)
            {
                var directive = directives.First(d => d.Name == dataFileDirective.Name);

                directive.Enabled = dataFileDirective.Enabled;
                directive.SortOrder = dataFileDirective.SortOrder;
            }
            
            return directives.OrderBy(d => d.SortOrder).ToList();
        }

        private static void SaveDirectives(List<Directive> directives)
        {
            var targetGroups = new Dictionary<CdmBuildTargetGroup, List<Directive>>();

            foreach (var directive in directives)
            {
                foreach (CdmBuildTargetGroup targetGroup in Enum.GetValues(typeof(CdmBuildTargetGroup)))
                {
                    if (string.IsNullOrEmpty(directive.Name) || !directive.Enabled) continue;
                    if (!directive.Targets.HasFlag(targetGroup)) continue;
                    if (!targetGroups.ContainsKey(targetGroup)) targetGroups.Add(targetGroup, new List<Directive>());

                    targetGroups[targetGroup].Add(directive);
                }
            }

            foreach (CdmBuildTargetGroup targetGroup in Enum.GetValues(typeof(CdmBuildTargetGroup)))
            {
                var symbols = "";

                if (targetGroups.ContainsKey(targetGroup))
                {
                    symbols = string.Join(";", targetGroups[targetGroup].Select(d => d.Name).ToArray());
                }

                PlayerSettings.SetScriptingDefineSymbolsForGroup(targetGroup.ToBuildTargetGroup(), symbols);
            }

            SaveDirectivesToDataFile(directives);
        }

        private static void SaveDirectivesToDataFile(List<Directive> directives)
        {
            int x = 0;
            directives.ForEach(d => d.SortOrder = x++);

            SaveDataToXmlFile(directives);
        }

        public static Directive GetDirective(string directiveName)
        {            
            return GetDirectivesFromXmlFile<Directive>().FirstOrDefault(d => d.Name.Equals(directiveName, StringComparison.OrdinalIgnoreCase));
        }

        public static void EnableDirective(string directiveName)
        {
            var directives = GetDirectivesFromXmlFile<Directive>();
            var directive = directives.FirstOrDefault(d => d.Name.Equals(directiveName, StringComparison.OrdinalIgnoreCase));

            if (directive == null)
            {
                Debug.LogErrorFormat("Directive '{0}' not found!", directiveName);
                return;
            }

            directive.Enabled = true;            
            SaveDirectives(directives);

            // also update the editor window            
            var window = Resources.FindObjectsOfTypeAll<CustomDefineManager>().LastOrDefault();
            if (window == null) return;
            {
                var windowDirective = window._directives.FirstOrDefault(d => d.Name == directive.Name);
                if (windowDirective == null) return;
                windowDirective.Enabled = true;
                window.Repaint();
            }
        }

        public static void DisableDirective(string directiveName)
        {
            var directives = GetDirectivesFromXmlFile<Directive>();
            var directive = directives.FirstOrDefault(d => d.Name.Equals(directiveName, StringComparison.OrdinalIgnoreCase));

            if (directive == null)
            {
                Debug.LogErrorFormat("Directive '{0}' not found!", directiveName);
                return;
            }
            directive.Enabled = false;
            SaveDirectives(directives);

            // also update the editor window            
            var window = Resources.FindObjectsOfTypeAll<CustomDefineManager>().LastOrDefault();
            if (window == null) return;
            {
                var windowDirective = window._directives.FirstOrDefault(d => d.Name == directive.Name);
                if (windowDirective == null) return;
                windowDirective.Enabled = false;
                window.Repaint();
            }
        }
    }
}