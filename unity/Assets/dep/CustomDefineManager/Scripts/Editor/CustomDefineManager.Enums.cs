﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;

namespace CustomDefineManagement
{
    public partial class CustomDefineManager
    {
        [Flags]
        public enum CdmBuildTargetGroup
        {
            Standalone = 1,
            Android = 2,
            Metro = 4,
            // ReSharper disable once InconsistentNaming
            tvOS = 8,
            PS4 = 16,
            // ReSharper disable once InconsistentNaming
            iOS = 32,
            XboxOne = 64,
            WebGL = 128,
          
        }
    }

    public static class BuildTargetGroupExtensions
    {
        public static BuildTargetGroup ToBuildTargetGroup(this CustomDefineManager.CdmBuildTargetGroup tg)
        {
            return (BuildTargetGroup) Enum.Parse(typeof(BuildTargetGroup), tg.ToString());
        }

        public static string ToIconName(this CustomDefineManager.CdmBuildTargetGroup tg)
        {
            return tg switch
            {
                CustomDefineManager.CdmBuildTargetGroup.iOS => "iPhone",
                _ => tg.ToString()
            };
        }

       
    }
}