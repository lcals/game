﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using Editor;
using UnityEngine.Events;

namespace CustomDefineManagement
{
    public partial class CustomDefineManager : EditorWindowExpand
    {
        private const string TitleName = "Custom Define Manager";

        [MenuItem("Tools/Custom Define Manager")]
        private static void Init()
        {
            var window = GetWindow<CustomDefineManager>("Custom Define Manager", true, typeof(SceneView));
            window.Show();
        }

        private List<Directive> _directives = new List<Directive>();
        private Color _guiColor;
        private Color _guiBackgroundColor;
        private List<Directive> _directivesToRemove;

        public CustomDefineManager()
        {
            GetXmlAssetPath = "Assets/dep/CustomDefineManager/Scripts/CustomDefineManagerData.xml";
        }

        protected override void OnEnable()
        {
            Reload();
        }

        private void OnGUI()
        {
            _guiColor = GUI.color;
            _guiBackgroundColor = GUI.backgroundColor;
            _directivesToRemove = new List<Directive>();

            var titleStyle = InitTitleStyle(TitleName, _guiColor);

            var tables = new List<(string name, int Width, int Height)>
            {
                ("Directive", 250, 20), ("Platforms", 370, 20), ("Enabled", 80, 20)
            };
            RenderTableHeader(tables, _guiColor);

            var textFieldStyle = new GUIStyle(EditorStyles.toolbarTextField)
            {
                alignment = TextAnchor.MiddleLeft,
                fixedHeight = 0,
                padding = new RectOffset(4, 4, 4, 4),
                fontSize = 12,
                margin = new RectOffset(0, 0, 1, 1)
            };

            var platformsStyles = new GUIStyle(titleStyle) {padding = new RectOffset(4, 4, 4, 4)};

            var removeButtonStyle = new GUIStyle(EditorStyles.toolbarButton)
            {
                normal = {textColor = Color.red}, fixedHeight = 0, margin = new RectOffset(0, 0, 1, 1)
            };

            var toggleStyle = new GUIStyle(EditorStyles.toggle) {alignment = TextAnchor.MiddleCenter, fixedWidth = 0};

            foreach (var directive in _directives)
            {
                EditorStyles.helpBox.alignment = TextAnchor.MiddleLeft;

                GUI.color = new Color(0.65f, 0.65f, 0.65f);

                EditorGUILayout.BeginHorizontal(titleStyle, GUILayout.Height(24), GUILayout.ExpandWidth(true));
                {
                    GUI.color = _guiColor;

                    if (GUILayout.Button(new GUIContent("X", "Remove this directive"), removeButtonStyle,
                        GUILayout.Width(31), GUILayout.Height(24)))
                    {
                        _directivesToRemove.Add(directive);
                    }

                    GUILayout.Space(4);

                    directive.Name = EditorGUILayout.TextField(directive.Name, textFieldStyle, GUILayout.Width(250),
                        GUILayout.Height(24));

                    GUILayout.Space(7);

                    EditorGUILayout.BeginHorizontal(platformsStyles, GUILayout.Height(24), GUILayout.Width(370));
                    {
                        foreach (CdmBuildTargetGroup targetGroup in Enum.GetValues(typeof(CdmBuildTargetGroup)))
                        {
                            var platformButtonStyle = new GUIStyle(EditorStyles.toolbarButton)
                            {
                                fontStyle = FontStyle.Bold
                            };

                            var hasFlag = directive.Targets.HasFlag(targetGroup);

                            if (!hasFlag)
                            {
                                GUI.backgroundColor = new Color(1, 1, 1, 0.25f);
                                GUI.color = new Color(1, 1, 1, 0.25f);
                            }
                            else
                            {
                                GUI.backgroundColor = new Color(0.5f, 1f, 0.5f, 1f);
                            }

                            GUIContent buttonContent;

                            var icon = EditorGUIUtility.IconContent("BuildSettings." + targetGroup.ToIconName());
                            if (icon != null)
                            {
                                buttonContent = new GUIContent(icon.image, targetGroup.ToString());
                            }
                            else
                            {
                                buttonContent = new GUIContent(targetGroup.ToString()[0].ToString(),
                                    targetGroup.ToString());
                            }

                            if (GUILayout.Button(buttonContent, platformButtonStyle, GUILayout.Width(24),
                                GUILayout.Height(18)))
                            {
                                if (hasFlag)
                                {
                                    directive.Targets &= ~targetGroup;
                                }
                                else
                                {
                                    directive.Targets |= targetGroup;
                                }
                            }

                            GUI.backgroundColor = _guiBackgroundColor;
                            GUI.color = _guiColor;
                        }
                    }
                    EditorGUILayout.EndHorizontal();

                    GUILayout.Space(7);


                    if (!directive.Enabled)
                    {
                        GUI.backgroundColor = new Color(1, 1, 1, 0.5f);
                    }

                    EditorGUILayout.BeginHorizontal(platformsStyles, GUILayout.Height(24), GUILayout.Width(80));
                    {
                        GUI.backgroundColor = _guiBackgroundColor;

                        GUILayout.Space(25);
                        directive.Enabled = GUILayout.Toggle(directive.Enabled, new GUIContent(), toggleStyle);
                    }
                    EditorGUILayout.EndHorizontal();
                }
                EditorGUILayout.EndHorizontal();
            }

            RenderNewDirectiveLine();

            var ends = new List<(string name, int Width, int Height, UnityAction unityEvent)>
            {
                ("Apply", 250, 20, () => { SaveDirectives(_directives); }),
                ("Revert", 370, 20, Reload)
            };
            RenderTableEnd(ends, _guiColor);


            if (!_directivesToRemove.Any()) return;
            foreach (var directiveToRemove in _directivesToRemove)
            {
                _directives.Remove(directiveToRemove);
            }
        }


        private void RenderNewDirectiveLine()
        {
            var directiveLineStyle = new GUIStyle(EditorStyles.toolbar)
            {
                fixedHeight = 0, padding = new RectOffset(8, 8, 0, 0)
            };

            var addButtonStyle = new GUIStyle(EditorStyles.toolbarButton)
            {
                fixedHeight = 0, margin = new RectOffset(0, 0, 1, 1)
            };

            GUI.color = new Color(0.75f, 0.75f, 0.75f);

            EditorGUILayout.BeginHorizontal(directiveLineStyle, GUILayout.Height(24));

            GUI.color = _guiColor;

            if (GUILayout.Button(new GUIContent("+", "Add new Directive"), addButtonStyle, GUILayout.Width(32),
                GUILayout.Height(24)))
            {
                var lastDirective = _directives.LastOrDefault();
                var newDirective = new Directive();

                if (lastDirective != null)
                {
                    newDirective.Targets = lastDirective.Targets;
                }

                _directives.Add(newDirective);
            }

            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
        }

        private void Reload()
        {
            _directives = LoadDirectives();
        }
    }
}