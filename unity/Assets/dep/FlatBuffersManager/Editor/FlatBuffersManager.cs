using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NPOI.XSSF.UserModel;
using UnityEditor;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace FlatBuffersManagement
{
    public class FlatBuffersManager : MonoBehaviour
    {
        private const string TestDataRootDataJson = "../unity_data/test_data/RootData.Json";
        private const string GenerateDataVersionPath = "Assets/dep/FlatBuffersManager/Editor/DataVersion.Json";
        private static Version _dataVersion = new Version();
        private static string _fullPath;
        public static int GetId = 0;

        [MenuItem("Tools/FlatBuffers/GenerateCode")]
        private static void GenerateCode()
        {
            NewDataVersion(true);
            var tables = LoadInfoFormXlsxFile();

            var stringBuilder = new StringBuilder(1024 * 100);
            stringBuilder.Append("namespace GameData;\n");
            tables.ForEach(x => x.AddEnum(stringBuilder));
            tables.ForEach(x => x.AddProperty(stringBuilder));
            stringBuilder.Append("table StringKey {\n");
            stringBuilder.Append("  key:int32(key);\n");
            stringBuilder.Append("  value:string;\n");
            stringBuilder.Append("}\n");
            stringBuilder.Append("table RootData {\n");
            tables.ForEach(x => x.AddTable(stringBuilder));
            stringBuilder.Append("  string_key:[StringKey];\n");
            stringBuilder.Append("  version:string;\n");
            stringBuilder.Append("}\n");
            stringBuilder.Append("root_type RootData;\n");
            stringBuilder.Append("file_identifier    \"data\";");
            stringBuilder.Append("file_extension \"bytes\";");
            File.WriteAllText(Table.GenerateFbsPath, stringBuilder.ToString());

            var arg =
                $" -o {Path.GetFullPath(Table.GeneratePath)}  --csharp --cs-gen-json-serializer  --gen-onefile --gen-object-api  {Path.GetFullPath(Table.GenerateFbsPath)} ";
            RumCmd(arg, Path.GetFullPath(Table.FlatcPath));
            arg =
                $" -o {Path.GetFullPath(Table.GenerateLuaPath)}  --lua   --gen-onefile  {Path.GetFullPath(Table.GenerateFbsPath)} ";
            RumCmd(arg, Path.GetFullPath(Table.FlatcPath));
            AssetDatabase.Refresh();
        }


        [MenuItem("Tools/FlatBuffers/ReadData")]
        private static void ReadData()
        {
            var newDataVersion = NewDataVersion();

            GetId = 0;
            var tables = LoadInfoFormXlsxFile(true);
            var stringBuilder = new StringBuilder(1024 * 100);
            var stringKey = new StringBuilder(1024 * 100);
            stringBuilder.Append("{\n");
            tables.ForEach(x => x.AddTableData(stringBuilder, stringKey));
            stringBuilder.Append("  \"string_key\": [\n");
            stringKey.Remove(stringKey.Length - 2, 1);
            stringBuilder.Append(stringKey);
            stringBuilder.Append("  ],\n");
            stringBuilder.Append($"  \"version\": \"{newDataVersion}\"\n");
            stringBuilder.Append("}\n");
            File.WriteAllText(Table.GenerateJsonPath, stringBuilder.ToString());
            var arg =
                $"  -o {Path.GetFullPath(Table.GenerateDataPath)}   -b {Path.GetFullPath(Table.GenerateFbsPath)}  {Path.GetFullPath(Table.GenerateJsonPath)} ";
            RumCmd(arg, Path.GetFullPath(Table.FlatcPath));
            AssetDatabase.Refresh();
        }


        [MenuItem("Tools/FlatBuffers/ReadExternalData")]
        private static void ReadExternalData()
        {
            var newDataVersion = NewDataVersion();
            GetId = 0;
            var tables = LoadInfoFormXlsxFile(true);
            var stringBuilder = new StringBuilder(1024 * 100);
            var stringKey = new StringBuilder(1024 * 100);
            stringBuilder.Append("{\n");
            tables.ForEach(x => x.AddTableData(stringBuilder, stringKey));
            stringBuilder.Append("  \"string_key\": [\n");
            stringKey.Remove(stringKey.Length - 2, 1);
            stringBuilder.Append(stringKey);
            stringBuilder.Append("  ],\n");
            stringBuilder.Append($"  \"version\": \"{newDataVersion}\"\n");
            stringBuilder.Append("}\n");
            File.WriteAllText(Path.GetFullPath(TestDataRootDataJson), stringBuilder.ToString());
            var arg =
                $"  -o {Path.GetFullPath("../unity_data/test_data/")}   -b {Path.GetFullPath(Table.GenerateFbsPath)}  {Path.GetFullPath(TestDataRootDataJson)} ";
            RumCmd(arg, Path.GetFullPath(Table.FlatcPath));
        }

        private static Version NewDataVersion(bool generate = false)
        {
            _fullPath = Path.GetFullPath(GenerateDataVersionPath);
            Version newDataVersion;
            if (File.Exists(_fullPath))
            {
                var json = File.ReadAllText(_fullPath);
                _dataVersion = JsonConvert.DeserializeObject<Version>(json,new VersionConverter());
                System.Diagnostics.Debug.Assert(_dataVersion != null, nameof(_dataVersion) + " != null");
                newDataVersion = generate
                    ? new Version(_dataVersion.Major, _dataVersion.Minor, _dataVersion.Build + 1,
                        _dataVersion.Revision)
                    : new Version(_dataVersion.Major, _dataVersion.Minor, _dataVersion.Build,
                        _dataVersion.Revision + 1);
            }
            else
            {
                newDataVersion = new Version(0,0,0,0);
            }

            File.WriteAllText(_fullPath,
                JsonConvert.SerializeObject(newDataVersion, new VersionConverter()));
            return newDataVersion;
        }


        private static void RumCmd(string arguments, string fileName)
        {
            using var flatc = new Process
            {
                StartInfo =
                {
                    Arguments = arguments,
                    UseShellExecute = false,
                    FileName = fileName,
                    CreateNoWindow = true,
                    RedirectStandardError = true,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    StandardErrorEncoding = Encoding.UTF8,
                    StandardOutputEncoding = Encoding.UTF8
                }
            };
            flatc.Start();
            Debug.Log(flatc.StandardOutput.ReadToEnd());
            Debug.Log(flatc.StandardError.ReadToEnd());
        }

        private static List<Table> LoadInfoFormXlsxFile(bool loadData = false)
        {
            var files = Directory.GetFiles(Path.GetFullPath("../unity_data"), "*.xlsx");
            var tables = new List<Table>();
            foreach (var file in files)
            {
                using var stream = new FileStream(file, FileMode.Open) {Position = 0};
                var xssWorkbook = new XSSFWorkbook(stream);
                for (var i = 0; i < xssWorkbook.NumberOfSheets; i++)
                {
                    var sheet = xssWorkbook.GetSheetAt(i);
                    var table = new Table {File = file, SheetName = sheet.SheetName};
                    string[,] property = null;
                    string[,] data = null;
                    for (var j = 0; j <= sheet.LastRowNum; j++)
                    {
                        var row = sheet.GetRow(j);
                        if (row == null) continue;
                        if (property == null)
                        {
                            property = new string[Table.DefinitionLine, row.LastCellNum];
                            data = new string[sheet.LastRowNum - Table.DefinitionLine + 1, row.LastCellNum];
                        }

                        for (var k = 0; k <= row.LastCellNum; k++)
                        {
                            var cell = row.GetCell(k);
                            if (cell == null) continue;
                            if (j < Table.DefinitionLine)
                            {
                                property[j, k] = cell.ToString();
                            }
                            else
                            {
                                if (loadData)
                                {
                                    data[j - Table.DefinitionLine, k] = cell.ToString();
                                }
                            }
                        }
                    }

                    if (property == null || property.Length <= 0)
                    {
                        continue;
                    }

                    var tableProperties = new List<string[]>();
                    for (var j = 0; j < property.GetLength(1); j++)
                    {
                        var temp = new string[Table.DefinitionLine];
                        for (var k = 0; k < property.GetLength(0); k++)
                        {
                            temp[k] = property[k, j];
                        }

                        if (temp.Any(string.IsNullOrEmpty))
                        {
                            Debug.LogWarning("PropertyDefinitionError File :" + file + " Sheet Name:" +
                                             sheet.SheetName + " Column " + j);
                            continue;
                        }

                        tableProperties.Add(temp);
                    }

                    table.AddPropertyAndData(tableProperties, data);
                    tables.Add(table);
                }
            }

            return tables;
        }
    }
}