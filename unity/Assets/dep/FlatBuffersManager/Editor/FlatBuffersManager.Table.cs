﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlatBuffersManagement
{
    public interface ITableProperty
    {
        public string ToPropertyCode();
        public string ToEnumCode();
    }

    public interface ITable
    {
        public void AddPropertyAndData(IEnumerable<string[]> tableProperties, string[,] data);
        public void AddTable(StringBuilder stringBuilder);
        public void AddProperty(StringBuilder stringBuilder);
        public void AddEnum(StringBuilder stringBuilder);
    }

    public class TableProperty
    {
        private readonly string _name;

        private readonly string[] _property;

        public TableProperty(string name, string[] property)
        {
            _name = name;
            _property = property;
        }


        public string ToPropertyCode()
        {
            if (_property[0] == "Id"
                && _property[1] == "Type"
                && _property[2] == "Desc")
            {
                return string.Empty;
            }

            switch (_property[1])
            {
                case "int8":
                case "uint8":
                case "bool":
                case "int16":
                case "uint16":
                case "int32":
                case "uint32":
                case "float32":
                case "int64":
                case "uint64":
                case "float64":
                    return $"  {_property[0]}:{_property[1]};\n";
                case "string":
                    return $"  {_property[0].ToLower()}_id:int32;\n";
                default:
                {
                    if (_property[1].StartsWith("enum_"))
                    {
                        return $"  {_property[0].ToLower()}:{_property[0]};\n";
                    }
                    else
                    {
                        throw new Exception("UnrecognizedType  Property :" + _name + "Type : " + _property[1]);
                    }
                }
            }
        }

        public string ToEnumCode()
        {
            if (!_property[1].StartsWith("enum_")) return string.Empty;
            var temp = _property[1].Split('_');
            var stringBuilder = new StringBuilder(1024 * 10);

            stringBuilder.Append($"enum {_property[0]}:uint32 (bit_flags) {{ ");

            for (var index = 1; index < temp.Length; index++)
            {
                var v = temp[index];
                stringBuilder.Append($"{v} = {index}");
                if (index != temp.Length - 1)
                {
                    stringBuilder.Append(", ");
                }
            }

            stringBuilder.Append("}\n");
            return stringBuilder.ToString();
        }


        public string ToDataCode(Func<string, int> addKeyAndValue, string data, bool ending)
        {
            T ParseTo<T>(object source)
            {
                try
                {
                    return (T) Convert.ChangeType(source, typeof(T));
                }
                catch
                {
                    throw new Exception("UnrecognizedType  Property :" + _name + "Type : " + _property[1] + "  Data :" +
                                        data);
                }
            }


            if (_property[0] == "Id"
                && _property[1] == "Type"
                && _property[2] == "Desc")
            {
                return $"      \"id\": {ParseTo<int>(data)}{(ending ? "" : ",")}\n";
            }

            switch (_property[1])
            {
                case "int8":
                    return $"      \"{_property[0].ToLower()}\": {ParseTo<sbyte>(data)}{(ending ? "" : ",")}\n";
                case "uint8":
                    return $"      \"{_property[0].ToLower()}\": {ParseTo<byte>(data)}{(ending ? "" : ",")}\n";
                case "int16":
                    return $"      \"{_property[0].ToLower()}\": {ParseTo<short>(data)}{(ending ? "" : ",")}\n";
                case "uint16":
                    return $"      \"{_property[0].ToLower()}\": {ParseTo<ushort>(data)}{(ending ? "" : ",")}\n";
                case "int32":
                    return $"      \"{_property[0].ToLower()}\": {ParseTo<int>(data)}{(ending ? "" : ",")}\n";
                case "uint32":
                    return $"      \"{_property[0].ToLower()}\": {ParseTo<uint>(data)}{(ending ? "" : ",")}\n";
                case "int64":
                    return $"      \"{_property[0].ToLower()}\": {ParseTo<long>(data)}{(ending ? "" : ",")}\n";
                case "uint64":
                    return $"      \"{_property[0].ToLower()}\": {ParseTo<ulong>(data)}{(ending ? "" : ",")}\n";
                case "bool":
                    return $"      \"{_property[0].ToLower()}\": {ParseTo<bool>(data)}{(ending ? "" : ",")}\n";
                case "float32":
                    return $"      \"{_property[0].ToLower()}\": {ParseTo<float>(data)}{(ending ? "" : ",")}\n";
                case "float64":
                    return $"      \"{_property[0].ToLower()}\": {ParseTo<double>(data)}{(ending ? "" : ",")}\n";
                case "string":
                    return $"      \"{_property[0].ToLower()}_id\": {addKeyAndValue.Invoke(data)}{(ending ? "" : ",")}\n";
                default:
                {
                    if (!_property[1].StartsWith("enum_"))
                        throw new Exception("UnrecognizedType  Property :" + _name + "Type : " + _property[1] +
                                            "  Data :" +
                                            data);
                    var property = _property[1].Split('_').Select(x => x.Trim()).Where(x => x != "enum");
                    var temp = data.Split(',').Select(x => x.Trim());
                    var enumerable = temp as string[] ?? temp.ToArray();
                    if (enumerable.Any(x => !property.Contains(x)))
                    {
                        throw new Exception("UnrecognizedType  Property :" + _name + "Type : " + _property[1] +
                                            "  Data :" +
                                            data);
                    }
                    return $"      \"{_property[0].ToLower()}\": \"{string.Join(", ", enumerable)}\"{(ending ? "" : ",")}\n";

                }
            }
        }
    }


    public class Table : ITable
    {
        public string File;
        public string SheetName;
        public const int DefinitionLine = 3;
        public const string GenerateDataPath = "Assets/data/";
        public const string GeneratePath = "Assets/Scripts/System/SResources/Gen/";
        public const string GenerateLuaPath = "Assets/Scripts/Lua/Gen";
        public const string GenerateFbsPath = "Assets/dep/FlatBuffersManager/Editor/RootData.fbs";
        public const string GenerateJsonPath = "Assets/dep/FlatBuffersManager/Editor/RootData.Json";
        public const string FlatcPath = "Assets/dep/FlatBuffersManager/Editor/flatc.exe";
        private readonly List<TableProperty> _tableProperties = new List<TableProperty>();
        private string[,] _data;

        public void AddPropertyAndData(IEnumerable<string[]> tableProperties, string[,] data)
        {
            foreach (var property in tableProperties)
            {
                _tableProperties.Add(new TableProperty(SheetName, property));
            }

            _data = data;
        }

        public void AddTable(StringBuilder stringBuilder)
        {
            stringBuilder.Append($"  {SheetName.ToLower()}:[{SheetName}];\n");
        }

        public void AddProperty(StringBuilder stringBuilder)
        {
            stringBuilder.Append($"/// {nameof(File)}:" + File + "\n");
            stringBuilder.Append($"/// {nameof(SheetName)}:" + SheetName + "\n");
            stringBuilder.Append("struct " + SheetName + " {\n");
            stringBuilder.Append("  id:int32(key);\n");
            foreach (var toPropertyCode in _tableProperties.Select(property => property.ToPropertyCode())
                .Where(toPropertyCode => !string.IsNullOrEmpty(toPropertyCode)))
            {
                stringBuilder.Append(toPropertyCode);
            }

            stringBuilder.Append("}\n");
        }

        public void AddEnum(StringBuilder stringBuilder)
        {
            stringBuilder.Append($"/// {nameof(File)}:" + File + "\n");
            stringBuilder.Append($"/// {nameof(SheetName)}:" + SheetName + "\n");
            foreach (var toEnumCode in _tableProperties.Select(property => property.ToEnumCode())
                .Where(toEnumCode => !string.IsNullOrEmpty(toEnumCode)))
            {
                stringBuilder.Append(toEnumCode);
            }
        }

        public void AddTableData(StringBuilder stringBuilder, StringBuilder stringKey)
        {
            int AddKeyAndValue(string value)
            {
                var key = FlatBuffersManager.GetId;
                stringKey.Append("    {\n");
                stringKey.Append($"      \"key\": {key},\n");
                stringKey.Append($"      \"value\": \"{value}\"\n");
                stringKey.Append("    },\n");
                FlatBuffersManager.GetId++;
                return key;
            }

            stringBuilder.Append($"  \"{SheetName.ToLower()}\": [\n");

            if (_data == null)
            {
                stringBuilder.Append("  ],\n");
                return;
            }

            for (var j = 0; j < _data.GetLength(0); j++)
            {
                stringBuilder.Append("    {\n");
                for (var i = 0; i < _tableProperties.Count; i++)
                {
                    stringBuilder.Append(_tableProperties[i]
                        .ToDataCode(AddKeyAndValue, _data[j, i], i == _tableProperties.Count - 1));
                }

                stringBuilder.Append(j == _data.GetLength(0) - 1 ? "    }\n" : "    },\n");
            }

            stringBuilder.Append("  ],\n");
        }
    }
}